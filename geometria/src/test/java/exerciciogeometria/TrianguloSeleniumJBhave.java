package br.ucsal.testequalidade.exerciciogeometria;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.chrome.ChromeDriver;

public class TrianguloSeleniumJBhave {
	
public static WebDriver driver;


	@Test
	public void entrar() {
		System.setProperty("webdriver.chrome.drive", "C:\\Users\\joaldo\\git\\geometria-2020-1\\geometria\\drivers\\chromedrive.exe");
		driver = new ChromeDriver();
		driver.get("C:\\Users\\joaldo\\git\\geometria-2020-1\\geometria\\src\\main\\webapp\\triangulo.html");
	}
	
	@Test
	public void selecionarHipotenusa() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Hipotenusa");

	}
	
	@Test
	public void selecionarCateto() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Cateto");

	}

	@Test
	public void informarCateto1(String catetoUm) {
		WebElement inputValorCateto1 = driver.findElement(By.id("cateto1"));
		inputValorCateto1.sendKeys(catetoUm);


	}

	@Test
	public void informarCateto2(String catetoDois) throws InterruptedException {
		WebElement inputValorCateto2 = driver.findElement(By.id("cateto2"));	
		inputValorCateto2.sendKeys(catetoDois);
		Thread.sleep(10000);
		

	}
	
	@Test
	public void informarHipotenusa(String hipotenusa) {
		WebElement inputHipotenusa = driver.findElement(By.id("hipotenusa"));
		inputHipotenusa.sendKeys(hipotenusa);
	}

	@Test
	public void calcular() {
		WebElement calcularBtn = driver.findElement(By.id("calcularBtn"));
		
		calcularBtn.click();
	}
	
	@Test
	public String obterHipotenusa() {
		WebElement valor = driver.findElement(By.id("hipotenusa"));
		return valor.getText();
	}

	@Test
	public String obterCateto() {
		WebElement valor = driver.findElement(By.id("cateto2"));
		return valor.getText();
	}
	
	@AfterAll
	public static void teardown() {
		driver.quit();
	}

}
