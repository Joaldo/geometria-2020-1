package br.ucsal.testequalidade.exerciciogeometria;


import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.chrome.ChromeDriver;

public class CalculoChromeTest extends TrianguloSeleniumJBhave {

	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver");
		driver = new ChromeDriver();
	}

}
