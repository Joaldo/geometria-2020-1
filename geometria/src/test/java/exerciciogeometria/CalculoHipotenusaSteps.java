package br.ucsal.testequalidade.exerciciogeometria;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CalculoHipotenusaSteps {

	TrianguloSeleniumJBhave triangulo = new TrianguloSeleniumJBhave();

	@Given("Dado que estou na pagina de calcular teorema de pitagoras")
	public void inicar() {
		triangulo.entrar();
	}

	@When("seleciono o tipo de calculo Hipotenusa")
	public void escolherHipotenusa() {
		triangulo.selecionarHipotenusa();
	}

	@When("informo $c1 para cateto1")
	public void informarCateto1(String c1) {
		triangulo.informarCateto1(c1);
	}

	@When("informo $c2 para cateto2")
	public void informarCateto2(String c2) throws InterruptedException {
		triangulo.informarCateto2(c2);
	}

	@When("solicito que o calculo seja realizado")
	public void calcular() {
		triangulo.calcular();
	}

	@When("a hipotenusa calculada sera $hipotenusa")
	public void verificarCalculo(String hipotenusa) {
		Assert.assertEquals(hipotenusa, triangulo.obterHipotenusa());
	}

}
