package br.ucsal.testequalidade.exerciciogeometria;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CalculoFirefoxTest extends TrianguloSeleniumJBhave{
	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.gecko.driver","./drivers/geckodriver");
		driver = new FirefoxDriver();
	}

}
