package br.ucsal.testequalidade.exerciciogeometria;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CalculoCatetoSteps {

	TrianguloSeleniumJBhave triangulo = new TrianguloSeleniumJBhave();
	
	@Given("que estou na pagina de calcular teorema de pitagoras")
	public void inicar() {
        triangulo.entrar();
	}

	
	@When("seleciono o tipo de calculo Cateto")
	public void escolherCateto() {
        triangulo.selecionarCateto();
	}

	@When("informo $c1 para cateto1")
	public void informarCateto1(String c1) {
		triangulo.informarCateto1(c1);
	}

	@When("informo $h para hipotenusa ")
	public void informarHipotenusa(String h) {
		triangulo.informarHipotenusa(h);;
	}
	
	@When("solicito que o calculo seja realizado")
	public void calcular() {
		triangulo.calcular();
	}

	
	@Then("o cateto2 calculado sera $c2")
	public void verificarCalculo(String c2) {
		Assert.assertEquals(c2, triangulo.obterHipotenusa());
	}

}
