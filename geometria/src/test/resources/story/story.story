Narrativa:
Como um usuario
desejo informar os dados dos catetos ou hipotenusa
de modo que eu possa saber os lados que faltam

Cenario: Calcular hipotenusa
Dado que estou na pagina de calcular teorema de pitagoras
Quando seleciono o tipo de calculo Hipotenusa
E informo 3 para cateto1
E informo 4 para cateto2
E solicito que o calculo seja realizado
Entao a hipotenusa calculada sera 5

Cenario: Calcular cateto
Dado que estou na pagina de calcular teorema de pitagoras
Quando seleciono o tipo de calculo Cateto
E informo 6 para cateto1
E informo 10 para hipotenusa
E solicito que o calculo seja realizado
Entao o cateto2 calculado sera 8


